package com.rabbani.locweb.coontroller;

import java.util.List;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.rabbani.locweb.entities.Location;
import com.rabbani.locweb.repos.LocationRepository;
import com.rabbani.locweb.service.LocationService;
import com.rabbani.locweb.util.EmailUtil;
import com.rabbani.locweb.util.ReportUtil;





@Controller
public class LocaitonController {
	
	@Autowired
	EmailUtil emailUtil;
	
	@Autowired
	LocationService service;
	
	@Autowired
	LocationRepository repository;
	@Autowired
	ReportUtil reportUtil;
	
	@Autowired
	ServletContext sc;
	
	
	@RequestMapping("/showCreate")
	public String showCreate() {
		return "createLocation";
	}
	
	@RequestMapping("/saveLoc")
	public String saveLocation(@ModelAttribute("location")Location location,ModelMap modelMap) {
		
	    Location locationSaved	=service.saveLocation(location);
	    String msg= "Location saved with id :"+ locationSaved.getId();
	    modelMap.addAttribute("msg", msg);
	   emailUtil.sendEmail("rishadrabbani@gmail.com", "Data Inserted", "Data Inserted Successfully");
		return "createLocation";
		
	}
	
	@RequestMapping("/displayLocations")
	public String displayLocations(ModelMap modelMap) {
		 List<Location> locations = service.getAllLocation();
		 modelMap.addAttribute("locations",locations);
		return "displayLocations";
	}
	
	@RequestMapping("/deleteLocation")
	public String deleteLocations(@RequestParam("id")  int id,ModelMap modelMap) {
		//Location location = service.getLocationById(id);
		Location location = new Location();
		location.setId(id);
		service.deletelocation(location);
		List<Location> locations = service.getAllLocation();
		 modelMap.addAttribute("locations",locations);
		return "displayLocations";
	}
	
	@RequestMapping("/showUpdate")
	public String showUpdate( @RequestParam("id") int id,ModelMap modelMap) {
		Location location = service.getLocationById(id);
		modelMap.addAttribute("location",location);
		return "updateLocation";
	}
	
	@RequestMapping("/updateLoc")
	public String updateLocation(@ModelAttribute("location")Location location,ModelMap modelMap) {
		service.updateLocation(location);
		List<Location> locations = service.getAllLocation();
		modelMap.addAttribute("locations",locations);
		
		return "displayLocations";
	}
	
	@RequestMapping("/generateReport")
	public String generateReport() {
		String path = sc.getRealPath("/");
		List<Object[]> data = repository.findTypeAndTypeCount();
		reportUtil.generatePieChart(path, data);
		return "report";
	}
	
	
	
}
