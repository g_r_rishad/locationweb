package com.rabbani.locweb.repos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.rabbani.locweb.entities.Location;


public interface LocationRepository extends JpaRepository<Location, Integer> {
	
	@Query(value="select type,count(type) from location group by type",nativeQuery=true)
	public List<Object[]> findTypeAndTypeCount();

}
