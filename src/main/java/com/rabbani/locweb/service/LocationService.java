package com.rabbani.locweb.service;

import java.util.List;

import com.rabbani.locweb.entities.Location;

public interface LocationService {
	Location saveLocation(Location location);
	Location updateLocation(Location location);
	void deletelocation(Location location);
	Location getLocationById(int id);
	List<Location> getAllLocation();
}
